'use strict';
module.exports = (sequelize, DataTypes) => {
  const StudentLearningDay = sequelize.define('StudentLearningDay', {
    studentId: DataTypes.INTEGER,
    learningdayId: DataTypes.INTEGER
  })
    StudentLearningDay.associate = function(models) {
      // define association here
      this.belongsTo(models.Student, {
        foreignKey: 'studentId',
        as: 'student'
      });
      this.belongsTo(models.LearningDay, {
        foreignKey: 'learningDayId',
        as: 'day'
      })
    }
  return StudentLearningDay;
};