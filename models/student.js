'use strict';

module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    name: DataTypes.STRING,
    age: DataTypes.STRING,
    schoolId: DataTypes.INTEGER,
    password: DataTypes.STRING,
    email: DataTypes.STRING
  })
    Student.associate = function(models) {
      // define association here
      Student.belongsTo(models.School, {
        foreignKey:'schoolId',
        as:'school'
      })

      Student.belongsToMany(models.LearningDay, {
        through: 'StudentLearningDays',
        foreignKey: 'studentId',
        as: 'learningDays'
      })
      console.log('test')
  };
  return Student;
};