'use strict';
module.exports = (sequelize, DataTypes) => {
  const LearningDay = sequelize.define('LearningDay', {
    day: DataTypes.STRING,
    date: DataTypes.DATE,
    isLearning: DataTypes.BOOLEAN
  }, {});
    LearningDay.associate = function(models) {
      // define association here
      LearningDay.belongsToMany(models.Student, {
        through: 'StudentLearningDays',
        foreignKey: 'learningdayId',
        as: 'students'
      })
    };
    return LearningDay;
};