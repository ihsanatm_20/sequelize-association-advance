'use strict';
module.exports = (sequelize, DataTypes) => {
  const School = sequelize.define('School', {
    name: DataTypes.STRING,
    address: DataTypes.STRING
  }, {})
    School.associate = function (models) {
      School.hasMany(models.Student, {
        foreignKey: 'schoolId',
        as: 'student'
      })
  };
  return School;
};