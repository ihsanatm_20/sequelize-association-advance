const Student = require('../models').Student
const bcrypt = require('bcrypt')
const JWT = require('jsonwebtoken')

class LoginController { 
    async login(req,res) {
    try {
        const password = req.body.password
        const user = await Student.findOne({ 
            where: {
                email: req.body.email
            }
        })

        if(user === null) {
            res.status(400).send({
                message: 'Email not found'
            }) 
        } else {
            const isMatch = bcrypt.compareSync(password,user.password)

            if(!isMatch) {
                res.status(401).send({
                    message: 'Unable to login'
                })
            } else {
                const generateToken = {
                    name: user.name,
                    email: user.email
                }
                const token = JWT.sign({ user: generateToken }, 'ihsanatmaji')
                res.status(200).send({
                    message: 'your email is right',
                    token: token
                })
            }
        }
    } catch (e) {
        res.status(500).send({
            message: 'Email salah'
        })
            console.log(e)
        }
    }
}

module.exports = LoginController