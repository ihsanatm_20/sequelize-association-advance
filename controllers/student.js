const Student = require('../models').Student
const LearningDay = require('../models').LearningDay
const bcrypt = require('bcrypt');

class studentController {
    async list(req,res) {
        try {
            const student = await Student.findAll({
                include:['school', {
                    model: LearningDay,
                    as: 'learningDays',
                    attributes: ["id", "day", "date", "isLearning"],
                    through: {
                        attributes:[],
                    },
                }]
            })
            res.status(200).send(student)
            console.log('test')
        } catch(e) {
            res.status(500).send(e)
            console.log(e)
        }
    }

    async create(req,res) {
        try {
            const password = req.body.password
            // const saltRounds = 10;
            // const salt = bcrypt.genSaltSync(saltRounds);
            const passwordHashed = await bcrypt.hashSync(password, 10); // bcrypt.hash("password", 10)
            const data = await Student.create({
                name: req.body.name,
                age: req.body.age,
                schoolId: req.body.schoolId,
                password: passwordHashed,
                email: req.body.email
            })
            res.status(200).send({
                message:"you sucess insert data",
                data: data
            })
        } catch (e) {
            res.status(500).send({
                message:'you wrong'
            })
            console.log(e)
        }
    }

    async delete(req,res) {
        try {
             await Student.destroy({
                where: {
                    id : req.params.id
                }
             }) 
                res.status(201).send({
                    message: `Data with ID ${req.params.id} id deleted`
                })
        } catch(e) {
                res.status(300).send({
                    message: 'Error'
                })
        }
    }
}

module.exports = studentController