const passport = require('passport');
const passportJWT = require('passport-jwt');

const JwtStrategy = passportJWT.Strategy;
const ExtractJwt = passportJWT.ExtractJwt;

passport.use(new JwtStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'ihsanatmaji'
},
  function (jwtPayload, done) {
    try {
      return done(null, jwtPayload.user);
    } catch (error) {
      return done(error);
    }
}));