'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('Students', [{
       name: 'Ihsan Atmaji',
       age: 27,
       schoolId: 1,
       createdAt: new Date(),
       updatedAt: new Date(),
      }], {});
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('Students', null, {});
  }
};
