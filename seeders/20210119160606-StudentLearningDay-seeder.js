'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.bulkInsert('StudentLearningDays', [{
       studentId: 2,
       learningdayId: 2
     }], {});
  },

  down: async (queryInterface, Sequelize) => {
     await queryInterface.bulkDelete('StudentLearningDays', null, {});
  }
};
