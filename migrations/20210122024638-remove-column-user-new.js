'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Students', 'updatedAt'),
      queryInterface.removeColumn('Students', 'createdAt')
    ])
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'Students',
        'updatedAt',
        {
          type: Sequelize.DATE,
        }
      ),
      queryInterface.addColumn(
        'Students',
        'createdAt',
        {
          type: Sequelize.DATE
        }
      ),

    ])
  }
};
