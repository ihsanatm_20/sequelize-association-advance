'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Students', 'email',
        { 
          type: Sequelize.STRING 
        });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('Students', 'email');
  }
};
