'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('StudentLearningDays', 'updatedAt');
    await queryInterface.removeColumn('StudentLearningDays', 'createdAt');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('StudentLearningDays', 'updatedAt', 
    {
        type: Sequelize.DATE,
    }
    ),
    await queryInterface.addColumn('StudentLearningDays', 'updatedAt',
    {
      type: Sequelize.DATE,
    }
    )
  }
};
