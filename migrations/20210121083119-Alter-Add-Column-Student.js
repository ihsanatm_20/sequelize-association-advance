'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
      await queryInterface.addColumn('Students', 'password',
       { type: Sequelize.STRING,
         allowNull: true  
      }
    );  
  },

  down: async (queryInterface, Sequelize) => {
        await queryInterface.removeColumn('Students', 'password');
  }
};
