const express = require('express')
const router = express.Router()
const studentController = require('../controllers/student')
const loginController = require('../controllers/login')
const passport = require('passport')

const st = new studentController()
const lg = new loginController()

router.get('/', passport.authenticate('jwt', { 'session': false }), st.list);
router.post('/', st.create);
router.delete('/:id', passport.authenticate('jwt', { 'session': false }), st.delete);
router.post('/login', lg.login);

module.exports = router
